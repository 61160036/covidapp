import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class questionWidget extends StatefulWidget {
  questionWidget({Key? key}) : super(key: key);

  @override
  _questionWidgetState createState() => _questionWidgetState();
}

class _questionWidgetState extends State<questionWidget> {
  var questions = [
    'มีใข้หรือหนาวสั่น',
    'มีอาการไอ',
    'มีอาการแน่นหน้าอก',
    'มีอาการเหนื่อยล้า',
    'ปวดกล้ามเนื้อหรือร่างกาย',
    'ปวดหัว',
    'ไม่ได้กลิ่นและรส',
    'เจ็บคอ',
    'คัดจมูกน้ำมูกไหล',
    'คลื่นไส้อาเจียน',
    'ท้องเสีย'
  ];
  var qusetionsValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Question')),
      body: Container(
        padding: EdgeInsets.all(15),
        child: ListView(
          children: [
            CheckboxListTile(
                value: qusetionsValues[0],
                title: Text(questions[0]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[0] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: qusetionsValues[1],
                title: Text(questions[1]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[1] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: qusetionsValues[2],
                title: Text(questions[2]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[2] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: qusetionsValues[3],
                title: Text(questions[3]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[3] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: qusetionsValues[4],
                title: Text(questions[4]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[4] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: qusetionsValues[5],
                title: Text(questions[5]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[5] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: qusetionsValues[6],
                title: Text(questions[6]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[6] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: qusetionsValues[7],
                title: Text(questions[7]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[7] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: qusetionsValues[8],
                title: Text(questions[8]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[8] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: qusetionsValues[9],
                title: Text(questions[9]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[9] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: qusetionsValues[10],
                title: Text(questions[10]),
                onChanged: (newValue) {
                  setState(() {
                    qusetionsValues[10] = newValue!;
                  });
                }),
            ElevatedButton(
                onPressed: () async {
                  await _saveQuestion();
                  Navigator.pop(context);
                },
                child: const Text('Save')),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var pref = await SharedPreferences.getInstance();
    var strQuestionValue = pref.getString('question_values') ??
        '[false, false, false, false, false, false, false, false, false, false, false]';
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        qusetionsValues[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var pref = await SharedPreferences.getInstance();
    pref.setString('question_values', qusetionsValues.toString());
  }
}
